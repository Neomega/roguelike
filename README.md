# roguelike

A small scifi-themed roguelike game

## License

The License is at <http://neomega.mit-license.org/>

## Build

### Dependencies

#### All Platforms

- POSIX.1-2008 (or newer) environment
- C compiler with C17 support
- OpenGL 3.3 or later
- GLFW 3
- GLEW

#### Linux and Non-Unix Platforms

- Either bmake or omake

If not in your platform's software repository, they can be found below:

> ###### For bmake: <http://www.crufty.net/help/sjg/bmake.html>
> ###### For omake: <https://github.com/ibara/make>

### Examples

#### Build and Install

##### *OpenBSD make and omake*

	$ make
	# make install

##### *bmake*

	$ bmake
	# bmake install

#### Dump target and variable information into a pager

##### *OpenBSD make and omake*

	$ make -p | less

##### *bmake*

	$ bmake -n -d g1 | less <2

## Troubleshoot

#### *fatal error: 'GL/glu.h' file not found*

Your CFLAGS is missing an -I path to a header, or you are missing a dependency.
If you know the dependency is already installed, try something like:

	$ find /usr -type f -path '*/include/*' -name 'glu.h' 2>/dev/null
	/usr/some_nonstandard_path/include/GL/glu.h
	$ CFLAGS="${CFLAGS} -I/usr/some_nonstandard_path/include" bmake

#### *ld: error: unable to find library -lGL*

Like above, you are either missing a dependency, or ld is missing a path. The
solution is very similar to a missing include path, but passed to the linker
through LDFLAGS instead of the compiler:

	$ find /usr -type f -path '*/lib/*' -name 'libGL.so*' 2>/dev/null
	/usr/some_nonstandard_path/lib/libGL.so.17.1
	$ LDFLAGS="${LDFLAGS} -L/usr/some_nonstandard_path/lib" bmake
