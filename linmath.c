/*
 * Linear math library for OpenGL.
 */
#include "linmath.h"
#include "error.h"

#include <math.h>
#include <string.h>

inline void
linmakeident(GLfloat buf[], GLuint size)
{
	for (GLuint i = 0, stride = 0; i < size; ++i, stride += size + 1)
		buf[stride] = 1.0f;
}

inline void
linadd(GLfloat buf[],
    const GLuint size, const GLuint stride,
    const GLfloat sum)
{
	for (GLuint i = 0; i < size * stride; ++i)
		buf[i] += sum;
}

inline void
linmult(GLfloat buf[],
    const GLuint size, const GLuint stride,
    const GLfloat mult)
{
	for (GLuint i = 0; i < size * stride; ++i)
		buf[i] *= mult;
}

inline void
linscale(GLfloat buf[],
    const GLuint size, const GLuint stride,
    const GLfloat scalar, const int scale)
{
	for (GLuint i = 0; i < size * stride; ++i) {
		if (scale == SCALE_UP) {
			if (buf[i] < 0)
				buf[i] -= scalar;
			else
				buf[i] += scalar;
		} else if (scale == SCALE_DOWN) {
			if (buf[i] < 0)
				buf[i] += scalar;
			else
				buf[i] -= scalar;
		}
	}
}
