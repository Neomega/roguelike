### OpenBSD make (omake)
inline_c.awk:
.	for FILE in ${INLF}
EXP_${FILE:U:S/./_/} != awk -f mk/inline_c.awk ${FILE}
DEF_INLINE += -DFILE_${FILE:C/^.*\///:U:S/./_/}=\""${EXP_${FILE:U:S/./_/}}"\"
.	endfor
