### bmake
inline_c.awk:
.	for FILE in ${INLF}
EXP_${FILE:tu:S/./_/} != awk -f mk/inline_c.awk ${FILE}
DEF_INLINE += -DFILE_${FILE:C/^.*\///:tu:S/./_/}=\""${EXP_${FILE:tu:S/./_/}}"\"
.	endfor
