/*
 * Standard-purpose string handling.
 */
#pragma once

#include <stddef.h>

/* Strings as records */
struct string {
	union {
		int len;
		unsigned int ulen;
		long llen;
	};
	char *str;
};

size_t get_longest_strlen(const char *const *, const short);
