/*
 * Filesystem operations.
 */
#pragma once

#define TXTRFILE_DEF {							\
	"texture/tile.data",						\
	"texture/hank.data"						\
}

long copy_file_to_buf(char **const, const char *const);
void init_dirs(void);
