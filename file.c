/*
 * Filesystem operations.
 */
#include "file.h"
#include "error.h"

#include <sys/types.h>
#include <errno.h>
#include <dirent.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
 * Allocate buf->str, and write contents and length of file at path into buf.
 */
long
copy_file_to_buf(char **const buf, const char *const path)
{
	FILE *const fp = fopen(path, "r");
	if (fp == NULL)
		return -1;

	if (fseek(fp, 0L, SEEK_END) == -1)
		return -1;
	long len = ftell(fp);
	if (len == -1)
		return -1;
	*buf = malloc(len + 1);
	if (*buf == NULL)
		return -3;
	if (fseek(fp, 0L, SEEK_SET) != 0)
		return -1;
	fread(*buf, len, 1, fp);
	if (ferror(fp) != 0)
		return -2;
	(*buf)[len] = '\0';

	fclose(fp);
	return len;
}

void
init_dirs(void)
{
	if (opendir(INSTSHARE) != NULL) {
		if (chdir(INSTSHARE) != 0)
			eputs(ETFATAL, "FILE", strerror(errno));
	} else if (errno == ENOENT) {
		/*
		 * Fall back to the directory the program binary is located
		 * in should INSTSHARE not exist (built but not installed).
		 */
		if (chdir(BUILDSHARE) != 0)
			eputs(ETFATAL, "FILE", strerror(errno));
	} else
		eputs(ETFATAL, "FILE", strerror(errno));
}
