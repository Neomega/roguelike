/*
 * Linear math library for OpenGL.
 */
#pragma once

#include <GL/gl.h>

enum scale { SCALE_UP, SCALE_DOWN };

//GLfloat lindot(const GLfloat [], const GLfloat []);
void linmakeident(GLfloat [], const GLuint);
void linadd(GLfloat [], const GLuint, const GLuint, const GLfloat);
void linmult(GLfloat [], const GLuint, const GLuint, const GLfloat);
void linscale(GLfloat [], const GLuint, const GLuint, const GLfloat, const int);
