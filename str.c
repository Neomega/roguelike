/*
 * Standard-purpose string handling.
 */
#include <string.h>

size_t
get_longest_strlen(const char *const *str, const short idx)
{
	size_t longest = strlen(*str);
	for (int i = 0; i < idx; ++i)
		if (strlen(*str) > longest)
			longest = strlen(*str);
	return longest;
}
