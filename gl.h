/*
 * OpenGL operations.
 */
#pragma once

#include "file.h"
#include "sym.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

struct glfw_window {
	GLFWwindow *win;
	int width;
	int height;
};

#define N_VAO 1
#define N_VBO 3
#define N_VSH 1
#define N_FSH 1
struct shader_info {
	GLuint prog;		/* Program */
	GLuint vao[N_VAO];	/* Vertex Array Objects */
	GLuint vbo[N_VBO];	/* Vertex Buffer Objects */
	GLuint vsh[N_VSH];	/* Vertex Shaders */
	GLuint fsh[N_FSH];	/* Fragment Shaders */
	GLuint *tex;		/* Textures */
};

#define VERT_DEF {							\
	{ -1.0f,  1.0f },						\
	{  1.0f,  1.0f },						\
	{  1.0f, -1.0f },						\
	{ -1.0f, -1.0f }						\
}
#define TEXCRD_DEF {							\
	{ 0, 0 },							\
	{ 1, 0 },							\
	{ 1, 1 },							\
	{ 0, 1 }							\
}
#define ELM_DEF {							\
	{ 0, 1, 2 },							\
	{ 2, 3, 0 }							\
}

void glfw_err_cb(int, const char *);
void glfw_key_cb(GLFWwindow *, int, int, int, int);
void init_glfw();
void init_glew();
void init_shaders();
void draw_loop(void);
void destroy_gl(void);
void init_gl(void);
