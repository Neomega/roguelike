### OpenBSD make (bmake/omake)
# Globals
PROG =		roguelike
LIBS =		m GL glfw GLEW
OBJS =		main error file gl linmath str
OBJS :=		${OBJS:S/$/.o/}
HDRS =		error file str gl linmath sym
HDRS :=		${HDRS:S/$/.h/}
GLSL !=		find glsl/*.glsl
TXTR =		hank
TXTR :=		${TXTR:S/$/.data/}

# Inline Files
INLF =		${GLSL}

# Packing List
PDIRS !=	find share/* -type d | sed 's/^share\/\(.*\)/\1/'
PFILES !=	find share/* -type f | sed 's/^share\/\(.*\)/\1/'
PLIST =		${PDIRS} ${PFILES}

# Platform
LOCALBASE ?=	/usr/local
UNAME !=	uname
.if ${UNAME} == "OpenBSD"
X11BASE =	/usr/X11R6
CFLAGS +=	-DOPENBSD_EXT \
		-I${X11BASE}/include
LDFLAGS +=	-L${X11BASE}/lib
.endif

# Build
CFLAGS +=	-I${LOCALBASE}/include
LDFLAGS +=	-L${LOCALBASE}/lib
BINPATH ?=	${LOCALBASE}/bin
MANPATH ?=	${LOCALBASE}/man
SHAREDIR ?=	${LOCALBASE}/share
INSTSHARE ?=	${SHAREDIR}/${PROG}

CC ?=		cc
CFLAGS +=	-Wall -Wextra -std=c17 \
		-I${LOCALBASE}/include
LDFLAGS +=	-L${LOCALBASE}/lib
.for LIB in ${LIBS}
LDLIBS +=	-l${LIB}
.endfor

all: ${PROG}

install: ${PROG} chkperms
	mkdir -m 755 ${INSTSHARE}
.	for DIR in ${PDIRS}
		mkdir -m 755 ${INSTSHARE}/${DIR}
.	endfor
.	for FILE in ${PFILES}
		install -m 644 share/${FILE} ${INSTSHARE}/${FILE}
.	endfor
	install -m 755 ${PROG} ${BINPATH}/${PROG}

deinstall: chkperms
	rm -f ${BINPATH}/${PROG}
.	for FILE in ${PFILES}
		rm -f ${INSTSHARE}/${FILE}
.	endfor
.	for DIR in ${PDIRS}
		rmdir ${INSTSHARE}/${DIR}
.	endfor
	rmdir ${INSTSHARE}

chkperms:
.	for DIR in ${BINDIR} ${SHAREDIR} ${MANPATH}
		@[ -w ${DIR} ] || {					\
			msg="User lacks write mode to ${DIR} ";		\
			msg="$${msg}(unprivileged?)\n---\n";		\
			printf "$${msg}" | fold -s >&2;			\
			exit 1;						\
		}
.	endfor

clean:
.	for FILE in ${PROG} ${OBJS}
		rm -f ${FILE}
.	endfor

###

${PROG}: ${OBJS}
	${LINK.c} -o ${.TARGET} ${LDLIBS} ${.ALLSRC}
.	ifndef DEBUG
		strip ${PROG}
.	endif

error.o: error.h
file.o: file.h error.h
	${COMPILE.c} \
	    -DINSTSHARE=\"${INSTSHARE}\" \
	    -DBUILDSHARE=\"${.CURDIR}/share\" \
	    ${.IMPSRC}
gl.o: ${INLF} gl.h error.h linmath.h
	${COMPILE.c} ${DEF_INLINE} ${.IMPSRC}
linmath.o: linmath.h error.h

error.h: sym.h
gl.h: file.h sym.h

${INLF}:	mk/inline_c.awk

.SUFFIXES: .c .o .h .glsl .awk .data

.MAIN: ${PROG}
.PHONY: install deinstall chkperms clean all

.if ${.MAKE} == "bmake"
.	include "mk/bmake.mk"
.else
.	include "mk/openbsd.mk"
.endif
