#version 330 core

in vec2 t_texcoord;

uniform sampler2D tex;
layout (location = 0) out vec4 out_color;

void
main(void)
{
	out_color = texture(tex, t_texcoord);
}
