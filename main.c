#include "gl.h"
#include "file.h"

#include <stdlib.h>

int
main(void)
{
//todo	init_thread_pool();
	init_dirs();

	init_glfw();
	init_glew();
	init_shaders();
	draw_loop();

	destroy_gl();
	exit(EXIT_SUCCESS);
}
