/*
 * Error handling api.
 */
#include "error.h"

#include <GL/gl.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void
eprintf(enum err_type et, const char *const err_hdr, const char *const fmt, ...)
{
	fprintf(stderr, "%s%s %s%s\n",
	    (et == ETFATAL) ? ANSI_RED : ANSI_YELLOW,
	    err_hdr,
	    (et == ETFATAL) ? "ERROR" : "WARNING",
	    ANSI_RESET
	);

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (et == ETFATAL)
		exit(EXIT_FAILURE);
}

void
eputs(enum err_type et, const char *const err_hdr, const char *const msg)
{
	fprintf(stderr, "%s%s %s%s\n",
	    (et == ETFATAL) ? ANSI_RED : ANSI_YELLOW,
	    err_hdr,
	    (et == ETFATAL) ? "ERROR" : "WARNING",
	    ANSI_RESET
	);

	fprintf(stderr, "%s\n", msg);

	if (et == ETFATAL)
		exit(EXIT_FAILURE);
}

inline void
chk_glerror(enum err_type et)
{
	const char *errstr = NULL;
	for (GLenum err = glGetError();
	    err != GL_NO_ERROR; err = glGetError())
		switch(err) {
		case GL_INVALID_ENUM:
			errstr = "Invalid enumeration";
			break;
		case GL_INVALID_VALUE:
			errstr = "Invalid value";
			break;
		case GL_INVALID_OPERATION:
			errstr = "Invalid operation";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			errstr = "Invalid framebuffer operation";
			break;
		case GL_OUT_OF_MEMORY:
			errstr = "Out of memory";
			break;
		case GL_STACK_UNDERFLOW:
			errstr = "Stack underflow";
			break;
		case GL_STACK_OVERFLOW:
			errstr = "Stack overflow";
			break;
		default:
			errstr = "Unknown error";
		}

	if (errstr != NULL)
		eputs(et, "GL", errstr);
}
