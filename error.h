/*
 * Error handling api.
 */
#pragma once

#include "sym.h"

enum err_type {
	ETWARN,		/* warning */
	ETFATAL		/* fatal */
};

void eprintf(enum err_type, const char *const, const char *const, ...);
void eputs(enum err_type, const char *const, const char *const);
void chk_glerror(enum err_type);
