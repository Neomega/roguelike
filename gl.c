/*
 * OpenGL operations.
 */
#include "gl.h"
#include "error.h"
#include "linmath.h"

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static struct glfw_window gl_win;
static struct shader_info sh_info;

const GLfloat vert_def[][2] = VERT_DEF;
const GLuint texcrd_def[][2] = TEXCRD_DEF;
const GLuint elm_def[][3] = ELM_DEF;
const char *txtrfiles[] = TXTRFILE_DEF;
struct square_primitive {
	GLfloat vert[NUM_ELS(vert_def)][NUM_ELS(vert_def[0])];
	GLuint texcrd[NUM_ELS(texcrd_def)][NUM_ELS(texcrd_def[0])];
	GLuint elm[NUM_ELS(elm_def)][NUM_ELS(elm_def[0])];
} static *sp;

void
glfw_err_cb(int err, const char *desc)
{
	(void)err;
	eputs(ETFATAL, "GLFW", desc);
}

void
glfw_key_cb(GLFWwindow *win, int key, int code, int act, int mods)
{
	(void)code;
	(void)mods;

	switch (key) {
	case GLFW_KEY_Q:
	case GLFW_KEY_ESCAPE:
		if (act == GLFW_PRESS)
			glfwSetWindowShouldClose(win, GLFW_TRUE);
		break;
	case GLFW_KEY_LEFT_BRACKET:
		if (act == GLFW_PRESS)
			linscale(*sp->vert,
			    NUM_ELS(sp->vert), NUM_ELS(*sp->vert),
			    0.1f, SCALE_DOWN);
		break;
	case GLFW_KEY_RIGHT_BRACKET:
		if (act == GLFW_PRESS)
			linscale(*sp->vert,
			    NUM_ELS(sp->vert), NUM_ELS(*sp->vert),
			    0.1f, SCALE_UP);
	}
}

void
init_glfw(void)
{
	if (!glfwInit())
		eputs(ETFATAL, "GLFW", "Failed to initialize!");

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	gl_win.win = glfwCreateWindow(480, 480, "vidoe gam", NULL, NULL);
	if (gl_win.win == NULL)
		eputs(ETFATAL, "GLFW", "Failed to create window!");

	glfwSetErrorCallback(glfw_err_cb);
	glfwMakeContextCurrent(gl_win.win);
	glfwSetKeyCallback(gl_win.win, glfw_key_cb);
	glfwSwapInterval(1);
}

void
init_glew(void)
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		eputs(ETFATAL, "GLEW", "Failed to initialize!");
}

void
init_shaders(void)
{
	sp = malloc(sizeof(struct square_primitive));
	memcpy(&sp->vert, vert_def, sizeof(vert_def));
	memcpy(&sp->texcrd, texcrd_def, sizeof(texcrd_def));
	memcpy(&sp->elm, elm_def, sizeof(elm_def));
	sh_info.tex = malloc(sizeof(GLuint) * NUM_ELS(txtrfiles));

	/* Setup Vertex Objects */
	glGenVertexArrays(N_VAO, sh_info.vao);
	glBindVertexArray(sh_info.vao[0]);

	glGenBuffers(N_VBO, sh_info.vbo);

	glBindBuffer(GL_ARRAY_BUFFER, sh_info.vbo[0]);
	glBufferData(GL_ARRAY_BUFFER,
	    sizeof(sp->vert), sp->vert, GL_STATIC_DRAW);
	glVertexAttribPointer(0,
	    2, GL_FLOAT, GL_FALSE, sizeof(*sp->vert), 0);
	glEnableVertexAttribArray(0);
	chk_glerror(ETFATAL);

	glBindBuffer(GL_ARRAY_BUFFER, sh_info.vbo[1]);
	glBufferData(GL_ARRAY_BUFFER,
	    sizeof(sp->texcrd), sp->texcrd, GL_STATIC_DRAW);
	glVertexAttribPointer(1,
	    2, GL_UNSIGNED_INT, GL_FALSE, sizeof(*sp->texcrd), 0);
	glEnableVertexAttribArray(1);
	chk_glerror(ETFATAL);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sh_info.vbo[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
	    sizeof(sp->elm), sp->elm, GL_STATIC_DRAW);
	chk_glerror(ETFATAL);

	/* Create Shaders */
	const GLchar *const sh_src[] = {
		FILE_VERT_GLSL,
		FILE_FRAG_GLSL
	};
	sh_info.vsh[0] = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(sh_info.vsh[0], N_VSH, sh_src + 0, NULL);
	sh_info.fsh[0] = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(sh_info.fsh[0], N_FSH, sh_src + 1, NULL);
	chk_glerror(ETFATAL);

	/* Compile Shaders */
	GLint status;
	glCompileShader(sh_info.vsh[0]);
	glGetShaderiv(sh_info.vsh[0], GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
		goto vert_shader_comp_failed;
	glCompileShader(sh_info.fsh[0]);
	glGetShaderiv(sh_info.fsh[0], GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
		goto frag_shader_comp_failed;

	/* Create Program and Attach Shaders */
	sh_info.prog = glCreateProgram();
	glAttachShader(sh_info.prog, sh_info.vsh[0]);
	glAttachShader(sh_info.prog, sh_info.fsh[0]);
	glLinkProgram(sh_info.prog);
	glUseProgram(sh_info.prog);
	chk_glerror(ETFATAL);

	/* Load Textures */
	char *txtrfile_buf;
	glGenTextures(NUM_ELS(txtrfiles), sh_info.tex);
	for (unsigned int i = 0; i < NUM_ELS(txtrfiles); ++i) {
		glBindTexture(GL_TEXTURE_2D, sh_info.tex[i]);
		if (copy_file_to_buf(&txtrfile_buf, txtrfiles[i]) < 0)
			eputs(ETFATAL, "FILE", strerror(errno));
		glTexImage2D(
		    GL_TEXTURE_2D, 0, GL_RGBA,
		    32, 32, 0, GL_RGBA,
		    GL_UNSIGNED_BYTE, txtrfile_buf
		);
		chk_glerror(ETFATAL);
		free(txtrfile_buf);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTexParameteri(GL_TEXTURE_2D,
		    GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,
		    GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		chk_glerror(ETFATAL);
	}
	return;

vert_shader_comp_failed:
{
	glGetShaderiv(sh_info.vsh[0], GL_INFO_LOG_LENGTH, &status);
	char *const buf = malloc(status);
	if (buf == NULL)
		eprintf(ETFATAL, "MALLOC", "Alloc vertex shader log: %s\n",
		    strerror(errno));
	glGetShaderInfoLog(sh_info.vsh[0], status, NULL, buf);
	eputs(ETFATAL, "GLSL VERTEX SHADER", buf);
}
frag_shader_comp_failed:
{
	glGetShaderiv(sh_info.fsh[0], GL_INFO_LOG_LENGTH, &status);
	char *const buf = malloc(status);
	if (buf == NULL)
		eprintf(ETFATAL, "MALLOC", "Alloc fragment shader log: %s\n",
		    strerror(errno));
	glGetShaderInfoLog(sh_info.fsh[0], status, NULL, buf);
	eputs(ETFATAL, "GLSL FRAGMENT SHADER", buf);
}
}

void
draw_loop(void)
{
	glClearColor(0.0f, 1.0f, 0.5f, 1.0f);
	while (!glfwWindowShouldClose(gl_win.win)) {
		glClear(GL_COLOR_BUFFER_BIT);

		glBindBuffer(GL_ARRAY_BUFFER, sh_info.vbo[0]);
		glfwPollEvents();
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vert_def), sp->vert);

		const int wchk = gl_win.width;
		const int hchk = gl_win.height;
		glfwGetFramebufferSize(gl_win.win,
		    &gl_win.width, &gl_win.height);
		if (wchk != gl_win.width || hchk != gl_win.height)
		    	glViewport(0, 0, gl_win.width, gl_win.height);

		glBindTexture(GL_TEXTURE_2D, sh_info.tex[0]);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindTexture(GL_TEXTURE_2D, sh_info.tex[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sh_info.vbo[2]);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glfwSwapBuffers(gl_win.win);
	}
}

void
destroy_gl(void)
{
	free(sp);
	free(sh_info.tex);

	for (int i = 0; i < N_VSH; ++i)
		glDeleteShader(sh_info.vsh[i]);
	for (int i = 0; i < N_FSH; ++i)
		glDeleteShader(sh_info.fsh[i]);
	glDeleteBuffers(N_VBO, sh_info.vbo);
	glDeleteVertexArrays(N_VAO, sh_info.vao);
	glDeleteProgram(sh_info.prog);

	glfwDestroyWindow(gl_win.win);
	glfwTerminate();
}
