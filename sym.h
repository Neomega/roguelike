/*
 * Miscellaneous symbols.
 */
#pragma once

#define NUM_ELS(x) (sizeof(x) / sizeof((x)[0]))

/*
 * ANSI color
 */
#define ESC		"\033"
#define ANSI_RESET	ESC "[0m"
#define ANSI_RED	ESC "[41m"
#define ANSI_GREEN	ESC "[42m"
#define ANSI_YELLOW	ESC "[43m"
#define ANSI_BLUE	ESC "[44m"
